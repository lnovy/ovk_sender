import logging
import os
import json

import ovk_sender
import weasyprint

logging.basicConfig(level=logging.DEBUG)


for f in ovk_sender.iterate_outfolder():
    with open(f) as file:
        data = json.load(file)
    pdf_file = f.replace('.json', '.pdf')
    wpdf = weasyprint.HTML(string=data['content']).write_pdf(pdf_file, ['style.css'])

