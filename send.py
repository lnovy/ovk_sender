import logging
import psycopg2
import os

import ovk_sender

logging.basicConfig(level=logging.DEBUG)

try:
    conn = psycopg2.connect(os.environ['DATABASE_URL'])
except Exception, e:
    logging.error("I am unable to connect to the database ...")
    raise e


for f in ovk_sender.iterate_outfolder():
    cur = conn.cursor()
    try:
        ovk_sender.process_single_file(f, cur)
        conn.commit()
        logging.info("%s processed" % f)
    except Exception, e:
        logging.exception(e)
        conn.rollback()