FROM debian:jessie
RUN apt-get update && apt-get install -y libpq-dev python-dev python-virtualenv libffi-dev git gcc libxml2-dev libxslt1-dev libcairo2 libcairo-gobject2 libpangocairo-1.0-0 
RUN mkdir /ovk_sender
COPY . /ovk_sender
WORKDIR /ovk_sender
RUN virtualenv .venv
RUN .venv/bin/pip install --upgrade setuptools
RUN .venv/bin/pip install -r requirements.txt
VOLUME /ovk_sender/out
