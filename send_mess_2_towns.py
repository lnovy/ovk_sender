import logging
import psycopg2
import os
from ovk_sender import town_info, ds

logging.basicConfig(level=logging.DEBUG)

try:
    conn = psycopg2.connect(os.environ['DATABASE_URL'])
except Exception, e:
    logging.error("I am unable to connect to the database ...")
    raise e

def sendMessage(obec_kod, content):
    ctx = town_info.resolve(obec_kod)
    if(ctx == None):
        logging.error("NENASEL jsem townId: %s" % townId)
        return
    
    ctx['predmet'] = os.environ.get('MESSAGE_SUBJECT', 'vzkaz od piratu').decode('utf-8')
    ctx['content'] = content.decode('utf-8')
    try:
        ds.send_message(ctx)
        logging.info("sent to %s ..." % ctx['nazev'])
    except Exception, e:
        logging.exception(e)

if __name__ == '__main__':
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    MAX_ORSKU = os.environ.get('MAX_ORSKU', 2)
    q = '''
    SELECT obec_kod FROM okrsky
    GROUP BY obec_kod
    HAVING COUNT(obec_kod) <= %s
    ''' % MAX_ORSKU
    cur.execute(q)
    reqs = cur.fetchall()
    cur.close()
    with open(os.environ['MESSAGE_FILE']) as f:
        content = f.read()
    for p in reqs:
        sendMessage(p['obec_kod'], content)