import logging
import psycopg2
import os

import ovk_sender

logging.basicConfig(level=logging.DEBUG)

try:
    conn = psycopg2.connect(os.environ['DATABASE_URL'])
except Exception, e:
    logging.error("I am unable to connect to the database ...")
    raise e

ovk_sender.prepare(conn, os.environ.get('VUSC', None),os.environ.get('BATCH_LIMIT', None))
