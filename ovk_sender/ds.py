from datoveschranky import sendmessage
import base64
import os

SEND_UNAME = os.environ.get('DS_SEND_UNAME')
SEND_PWD = os.environ.get('DS_SEND_PWD')

def prepare_message(data):
    return {
        'recpt': data['ds'],
        'subj': data['predmet'],
        'text': data['content'],
        'uname': SEND_UNAME,
        'pwd': SEND_PWD
    }

def send_message(data, content_type='text/plain', filename='zprava.txt'):
    textAttach = base64.standard_b64encode(data['content'])
    attachements = [
        (content_type, filename, textAttach)
    ]
    res = sendmessage.send(data['ds'], SEND_UNAME, SEND_PWD, data['predmet'], attachements)
    try:
        if(int(res.status.dmStatusCode) == 0):
            return (res.status.dmStatusMessage, res.data)
        else:
            raise Exception(str(res.status))
    except:
        raise Exception(str(res.status))
