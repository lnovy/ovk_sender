# coding=utf-8

import codecs
import glob
import json
import logging
import os

import psycopg2.extras

import delegace
import ds
import town_info

from unidecode import unidecode

OUTFOLDER = os.environ.get('OUTFOLDER', 'out')
if(not os.path.exists(OUTFOLDER)):
    os.mkdir(OUTFOLDER)


def prepare(conn, vusc, limit):
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    q = 'SELECT DISTINCT urad_kod FROM okrsky '
    if (vusc):
        q += ' WHERE vusc_kod = %s' % (vusc)
    if(limit):
        q += ' LIMIT %s;' % (limit)
    cur.execute(q)
    reqs = cur.fetchall()
    cur.close()
    for p in reqs:
        prepare_single_town(conn, p['urad_kod'])


def prepare_single_town(conn, townId):
    cur = conn.cursor(cursor_factory = psycopg2.extras.RealDictCursor)
    q = """
    SELECT *
    FROM public.registrace r
    LEFT JOIN okrsky o
    ON r.okrsek = o.kod
    WHERE datum_odeslani is NULL
    AND urad_kod = %s
    ORDER BY o.cislo
    """
    cur.execute(q, (townId, ))
    reqs = cur.fetchall()
    cur.close()
    if(reqs == []):
        return
    ctx = town_info.resolve(townId)

    if(ctx == None):
        logging.error("NENASEL jsem townId: %s" % townId)
        return

    ctx['content'] = delegace.render(reqs, ctx).decode('utf-8')
    ctx['predmet'] = u'Delegace do OVK'
    ctx['delegace'] = [d['id'] for d in reqs]

    folderName = os.path.join(OUTFOLDER, unidecode(ctx['kraj']))
    if(not os.path.exists(folderName)):
        os.mkdir(folderName)
    fname = os.path.join(folderName, ctx['zkratka'] + '.json')
    with codecs.open(fname, "w", "utf-8") as f:
        json.dump(ctx, f, ensure_ascii=False, indent=4, separators=(',', ': '))
    logging.info("%s written ..." % ctx['nazev'])


def iterate_outfolder():
    for f in glob.glob(OUTFOLDER + '/*/*.json'):
        yield f


def process_single_file(fle, cur):
    with open(fle) as f:
        data = json.load(f)

    pdf_file = fle.replace(".json", ".pdf")
    with open(pdf_file) as f:
        content = f.read()

    data['content'] = content
    ds.send_message(data, 'application/pdf', 'dopis.pdf')

    cur.execute('''
        UPDATE registrace SET datum_odeslani=now()
        WHERE id IN (%s)
    ''' % ','.join([str(i) for i in data['delegace']]))
