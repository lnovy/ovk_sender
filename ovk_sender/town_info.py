import os
import untangle

data = {}   # hack to allow global like behavior

def _download():
    import urllib2
    addr = 'https://seznam.gov.cz/ovm/datafile.do?format=xml&service=seznamovm'
    req = urllib2.urlopen(addr)
    if(req.code != 200):
        raise 'cannot read seznam OVM'
    return req.read()

def _init():
    if(not os.path.exists('.seznamovm.xml')):
        xmlfile = _download()
        with open('.seznamovm.xml', 'w') as f:
            f.write(xmlfile)
        data['parse'] = untangle.parse(xmlfile)
    else:
        with open('.seznamovm.xml') as f:
            data['parse'] = untangle.parse(f.read())


def resolve(kod):
    if(data == {}):
        _init()

    for s in data['parse'].SeznamOvmIndex.children:
        try:
            if s.PravniForma['type'] == '801' and s.AdresaUradu.ObecKod.cdata == str(kod):
                return {
                    'ds': s.IdDS.cdata,
                    'nazev': s.Nazev.cdata,
                    'zkratka': s.Zkratka.cdata,
                    'kod': kod,
                    'kraj': s.AdresaUradu.KrajNazev.cdata
                }
        except IndexError:
            pass
    return {
        'ds': '',
        'nazev': str(kod),
        'zkratka': str(kod),
        'kod': kod,
        'kraj': 'Nenalezeno'
    }
