# coding=utf-8
import os

message = """
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <p>
    %s<br />
    k rukám paní starostky - pana starosty
    </p>
    <p>
V souladu s ustanovením par. 17 odst. 2 a 3 zák. č. 130/2000 Sb.
%s,
jejíž kandidátní listina byla zaregistrována pro volby do zastupitelstva kraje %s,
deleguje níže uvedené členy do okrskových volebních komisí (OVK)
Vaší obce/města:
    </p> 
    <table>
      <tr>
        <th>OVK, do které má být zařazen</td>
        <th>Jméno a příjmení</td>
        <th>Rodné číslo</td>
        <th>Místo, kde je přihlášen k trvalému pobytu</td>
        <th>Telefon</th>
        <th>E-mail</th>
        <th>Datová schránka</th>
      </tr>
      %s
    </table> 
    <p>
    S pozdravem<br />
    %s<br />
    zmocněnec<br />
    (podpis ověřen odesláním datovou schránkou)
    </p>
  </body>
</html>
"""

member = """
<tr class="row ">
  <td>%(cislo)s</td>
  <td>%(jmeno)s %(prijmeni)s</td>
  <td>%(rc)s</td>
  <td>%(ulice)s<br />%(psc)s %(mesto)s</td>
  <td>%(telefon)s</td>
  <td>%(email)s</td>
  <td>%(datovka)s</td>
</tr>
"""

def render(data, townInfo):
    members = []
    for d in data:
        members.append(member % d)
    return message % (townInfo['nazev'].encode('utf-8'),os.environ.get('STRANA', 'politická strana Česká pirátská strana'),townInfo['kraj'].encode('utf-8'), '\n'.join(members), os.environ.get('PODPIS', 'V. Tobias Esner'))
