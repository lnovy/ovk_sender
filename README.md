# ovk_sender

Projde DB ovk a pripravi data pro odeslani ...

## instalace

```
sudo aptitude install -y libpq-dev python-dev python-virtualenv
git clone https://gitlab.com/pirati-cz/ovk_sender
cd ovk_sender
virtualenv .venv
.venv/bin/pip install -r requirements.txt
```

## spusteni prepare faze

Rizeni pres 2 env promenne:
- DATABASE_URL: connection string to DB
- OUTFOLDER: folder kde se objevy vysledek ... (vytvori se v pripade potreby)

```
OUTFOLDER=/tmp/ovk DATABASE_URL=postgresql://ovk:seCRet@localhost/ovkdb python prepare.py
```

## spusteni send faze

Rizeni pres ty same envvars + :
- DS_SEND_UNAME: usename odesilatele
- DS_SEND_PWD: heslo odesilatele

## custom message sender

Posle zpravu obcim s max MAX_ORSKU okrskama.
Nastaveni skrz envvars:
- DATABASE_URL: connection string to DB
- MAX_ORSKU: maximum okrsku pro vyber obce (default 2)
- MESSAGE_SUBJECT: predmet zpravy
- MESSAGE_FILE: cesta k souboru se zpravou

## pouziti dockeru

Dockerfile je prilozen.

```
docker build -t ovksender .
mkdir out
chcon -t svirt_sandbox_file_t out
docker run -it -e DATABASE_URL=xxx -v `pwd`/out:/ovk_sender/out ovksender 
```
